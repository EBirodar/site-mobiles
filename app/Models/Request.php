<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    use HasFactory;
    protected $table='requests';

    #region all relation methods
    public function preRequest()
    {
        return  $this->belongsTo(PreRequest::class);
    }

    public function transfer()
    {
        return $this->morphMany(Transfer::class, 'transferable');
    }
    public function mark()
    {
        return  $this->belongsTo(Mark::class);
    }
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }
    #endregion
}

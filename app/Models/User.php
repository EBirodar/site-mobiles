<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    #region protects
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $table='users';
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];
    #endregion

    #region all relation methods
    public function warehouse()
    {
        return $this->morphMany(Warehouse::class, 'warehouseable');
    }

    public function transfer()
    {
        return $this->morphMany(Transfer::class, 'transferable');
    }
    public function currenciable()
    {
        return $this->morphMany(Currenciable::class, 'currenciable');
    }

    public function employee()
    {
        return $this->hasMany(Employee::class);
    }
    public function attendance()
    {
        return $this->hasMany(Attendance::class);
    }
    public function client()
    {
        return $this->hasOne(Client::class);
    }

    public function shipment()
    {
        return $this->hasMany(Shipment::class);
    }
    public function order()
    {
        return $this->hasMany(Order::class);
    }
    #endregion

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shipment extends Model
{
    use HasFactory;
    protected $table='shipments';

    #region all relation methods
    public function product()
    {
        return $this->hasMany(Product::class);
    }
    public function productCode()
    {
        return $this->hasMany(ProductCode::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }
    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    #endregion
}

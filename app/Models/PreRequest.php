<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PreRequest extends Model
{
    use HasFactory;
    protected $table='pre_requests';


    #region all relation methods
    public function request()
    {
        return $this->hasMany(Request::class);
    }

    public function transfer()
    {
        return $this->morphMany(Transfer::class, 'transferable');
    }

    public function mark()
    {
        return $this->belongsTo(Mark::class);
    }
    #endregion
}

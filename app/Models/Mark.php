<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mark extends Model
{
    use HasFactory;
    protected $table='marks';

    #region all relation methods
    public function request()
    {
        return $this->hasMany(Request::class);
    }

    public function product()
    {
        return $this->hasMany(Product::class);
    }
    public function type()
    {
        return $this->belongsTo(Type::class);
    }
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }
    #endregion
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PriceTypeWarehouse extends Model
{
    use HasFactory;
    protected $table='price_type_warehouses';
    protected $fillable=[
        'price_type_id',
        'warehouse_id',
        'currency_id',
        'mark_id',
        'bonus',
        'price'
    ];

    # region all relation methods
    public function priceType()
    {
        return $this->belongsTo(PriceType::class);
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }
    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }
    public function mark()
    {
        return $this->belongsTo(Mark::class);
    }
    #endregion


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;
    protected $table='clients';
    protected $fillable=[
        'code',
        'region_id',
        'address',
        'user_id',
        'note'
    ];

    #region all relation methods
//    public function region()
//    {
//        return $this->belongsTo(Region::class);
//    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function currenciable()
    {
        return $this->morphMany(Currenciable::class, 'currenciable');
    }
    public function shipment()
    {
        return $this->hasMany(Shipment::class);
    }
    public function order()
    {
        return $this->hasMany(Order::class);
    }
    #endregion
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Currenciable extends Model
{
    use HasFactory;
    protected $table='currenciables';

    #region all relation methods
    public function currenciable()
    {
        return $this->morphTo();
    }

    public function currency()
    {
        return $this->belongsTo(Currenciable::class);
    }



    #endregion
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $table='orders';
    #region all relation methods

    public function productCode()
    {
        return $this->hasMany(ProductCode::class);
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }
    public function client()
    {
        return $this->belongsTo(Client::class);
    }
    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    #endregion
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    use HasFactory;
    protected $table='order_products';
    #region all relation methods
    public function order()
    {
        return $this->belongsTo(User::class);
    }
    public function product()
    {
        return $this->belongsTo(User::class);
    }
    #endregion
}

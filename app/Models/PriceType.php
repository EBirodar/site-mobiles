<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PriceType extends Model
{

    use HasFactory;
    protected $table='price_types';
    protected $fillable=[
        'name'
    ];

    public function priceTypeWarehouse()
    {
        return $this->hasMany(PriceTypeWarehouse::class);
    }
    #region all relation methods
    #endregion
}

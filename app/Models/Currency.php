<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    use HasFactory;
    protected $table='currencies';


    #region all relation methods
    public function currenciable()
    {
        return $this->hasMany(Currenciable::class);
    }
    public function moneyInvoice()
    {
        return $this->hasMany(MoneyInvoice::class);
    }
    public function moneyTransfer()
    {
        return $this->hasMany(MoneyTransfer::class);
    }
    public function order()
    {
        return $this->hasMany(Order::class);
    }

    #endregion
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    use HasFactory;
    protected $table='types';

    #region all relation methods
    public function mark()
    {
        return $this->hasMany(Mark::class);
    }
    #endregion
}

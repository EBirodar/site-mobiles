<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table='products';

    #region all relation methods
    public function productTransfer()
    {
        return $this->hasMany(ProductTransfer::class);
    }
    public function mark()
    {
        return $this->belongsTo(Mark::class);
    }
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }
    public function shipment()
    {
        return $this->belongsTo(Shipment::class);
    }

    public function productCode()
    {
        return $this->hasMany(ProductCode::class);
    }
    #endregion
}

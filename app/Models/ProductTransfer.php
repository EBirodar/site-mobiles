<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductTransfer extends Model
{
    use HasFactory;
    protected $table='product_transfers';


    #region all relation methods
    public function transfer()
    {
        return $this->morphMany(Transfer::class, 'transferable');
    }
    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    #endregion
}

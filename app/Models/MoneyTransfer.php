<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MoneyTransfer extends Model
{
    use HasFactory;
    protected $table='money_transfers';


    #region all relation methods
    public function transfer()
    {
        return $this->morphMany(Transfer::class, 'transferable');
    }
    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }
    #endregion
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    use HasFactory;
    protected $table='Warehouses';

    #region all relation methods
    public function group()
    {
        return  $this->belongsTo(Group::class);

    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function transfer()
    {
        return $this->belongsTo(Transfer::class);
    }

    public function request()
    {
        return $this->hasMany(Request::class);
    }
    public function product()
    {
        return $this->hasMany(Product::class);
    }
    public function shipment()
    {
        return $this->hasMany(Shipment::class);
    }
    public function order()
    {
        return $this->hasMany(Order::class);
    }
    #endregion

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MoneyInvoice extends Model
{
    use HasFactory;
    protected $table='money_invoices';

    #region all relation methods
    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }
    #endregion
}

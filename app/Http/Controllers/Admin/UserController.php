<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(){
        $users = User::where('status', 1)->paginate(10);
//        dd($users);
        $num = 0;
        return view("admin.users.index", ['users' => $users, 'num' => $num]);
    }
}

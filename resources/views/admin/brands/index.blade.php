@extends('layouts.admin ')
@section('content')
    <div class="container">

        <div class="row ">
            <div class="col-md-6">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="" method="post" action="{{route('admin.brands.store')}}">
                    @csrf
                    <table class="table">
                        <tbody>
                            <tr>
                                <td  >
                                    <div class="mb-3" >
{{--                                        <label for="name" class="form-label">{{__('Brand name')}}</label>--}}
                                        <input type="text" placeholder="brand name..." class="form-control" id="name" name="name"  value="{{old('name')}}">
                                    </div>
                                </td>
                                <td>
                                    <button type="submit" class="btn btn-primary">{{__('Add brand ')}}</button>
                                </td>

                            </tr>
                        </tbody>

                    </table>
                </form>
            </div>
        </div>



        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <h4 class="card-title">Table Edits</h4>
                        <p class="card-title-desc">Table Edits is a lightweight jQuery plugin for making table rows editable.</p>

                        <div class="table-responsive">
                            <table class="table table-editable table-nowrap align-middle table-edits">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($brands as $brand)
                                <tr data-id="{{$brand->id}}">
                                    <th >{{($brands->currentpage()-1)*$brands->perpage()+$loop->index+1}}</th>
                                    <td data-field="id" style="width: 80px">{{$brand->id}}</td>
                                    <td id="name" data-field="name">{{$brand->name}}</td>
                                    <td style="width: 100px">
                                        <a id="myBtn" onclick="TableEditable({{$brand->id}})" class="btn btn-outline-secondary btn-sm edit" title="Edit">
                                            <i class="fas fa-pencil-alt"></i>
                                        </a>
                                    </td>
                                    <td>

                                        <form method="post" class=" ml-5" action="{{route('admin.brands.destroy',['brand'=>$brand->id])}}">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" class="btn btn-danger">

                                                <span>DELETE&nbsp</span>
                                                <i class="fas fa-trash-alt" aria-hidden="true"></i>
                                            </button>

                                        </form>
                                    </td>

                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row ">
{{--            <div class="col-md-10">--}}
{{--                <table class="table table-dark table-striped">--}}
{{--                    <caption>List of brands</caption>--}}
{{--                    <thead>--}}
{{--                    <tr>--}}
{{--                        <th scope="col">#</th>--}}
{{--                        <th colspan="2">{{_('name')}}</th>--}}
{{--                        <th colspan="2"  class="text-center pr-5" scope="col">{{__('Actions')}}</th>--}}

{{--                    </tr>--}}
{{--                    </thead>--}}

{{--                    <tbody>--}}

{{--                    @foreach($brands as $brand)--}}
{{--                        <tr>--}}
{{--                            <form class="" method="post" action="{{route('admin.brands.edit',$brand->id)}}">--}}
{{--                            <th >{{($brands->currentpage()-1)*$brands->perpage()+$loop->index+1}}</th>--}}
{{--                            <td>--}}
{{--                                <span id="span_{{$brand->id}}">--}}
{{--                                    {{$brand->name}}--}}
{{--                                </span>--}}
{{--                                <input type="text" placeholder="brand name..." class="form-control hidden" id="name" name="name"  value="{{$brand->name}}">--}}
{{--                            </td>--}}
{{--                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>--}}
{{--                            <td class="">--}}
{{--                                <div class="d-flex flex-row-reverse">--}}
{{--                                    <button class=" mr-2 btn btn-primary text-white ml-5">--}}
{{--                                        <span >UPDATE&nbsp</span>--}}
{{--                                        <i class="fas fa-pencil-alt" aria-hidden="true"></i>--}}
{{--                                    </button>--}}
{{--                                </div>--}}

{{--                            </td>--}}
{{--                            </form>--}}
{{--                            <td>--}}

{{--                                <form method="post" class=" ml-5" action="{{route('admin.brands.destroy',['brand'=>$brand->id])}}">--}}
{{--                                    @method('DELETE')--}}
{{--                                    @csrf--}}
{{--                                    <button type="submit" class="btn btn-danger">--}}

{{--                                        <span>DELETE&nbsp</span>--}}
{{--                                        <i class="fas fa-trash-alt" aria-hidden="true"></i>--}}
{{--                                    </button>--}}

{{--                                </form>--}}
{{--                            </td>--}}
{{--                        </tr>--}}

{{--                    @endforeach--}}
{{--                    </tbody>--}}

{{--                </table>--}}
{{--            </div>--}}
        </div>



        {{$brands->links()}}

        <div class="row ">
{{--            <div class="col-md-6">--}}
{{--                @if ($errors->any())--}}
{{--                    <div class="alert alert-danger">--}}
{{--                        <ul>--}}
{{--                            @foreach ($errors->all() as $error)--}}
{{--                                <li>{{ $error }}</li>--}}
{{--                            @endforeach--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                @endif--}}
{{--                <form class="" method="post" action="{{route('admin.brands.update')}}">--}}
{{--                    @csrf--}}
{{--                    <table class="table">--}}
{{--                        <tbody>--}}
{{--                        <tr>--}}
{{--                            <td  >--}}
{{--                                <div class="mb-3" >--}}
{{--                                    --}}{{--                                        <label for="name" class="form-label">{{__('Brand name')}}</label>--}}
{{--                                    <input type="text" placeholder="brand name..." class="form-control" id="name" name="name"  value="{{old('name')}}">--}}
{{--                                    <input type="text" placeholder="brand name..." class="form-control hidden" id="name" name="name"  value="{{old('name')}}">--}}
{{--                                </div>--}}
{{--                            </td>--}}
{{--                            <td>--}}
{{--                                <button type="submit" class="btn btn-primary">{{__('Add brand ')}}</button>--}}
{{--                            </td>--}}

{{--                        </tr>--}}
{{--                        </tbody>--}}

{{--                    </table>--}}
{{--                </form>--}}
{{--            </div>--}}
        </div>

    </div>







    <script>
        // function edit(id, name){
        //     console.log(id, name)
        // }
        let i;

        function TableEditable(id){
             i =id;
            // console.log(i);
<!--            --><?php //$id="<script>document.write(this->i)</script>";?>


            let name = document.querySelector('#name');

            // Creating a XHR object
            let xhr = new XMLHttpRequest();
            let url = '{{route('admin.brands.update',"document.write(this->i)")}}';
            console.log(url);
            // open a connection
            xhr.open("POST", url, true);

            // Set the request header i.e. which type of content you are sending
            xhr.setRequestHeader("Content-Type", "application/json");

            // Create a state change callback
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 && xhr.status === 200) {

                    // Print received data from server
                    result.innerHTML = this.responseText;

                }
            };

            // Converting JSON data to string
            var data = JSON.stringify({ "name": name.value });

            // Sending data with the request
            xhr.send(data);
        }
    </script>
@endsection



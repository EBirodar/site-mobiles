@extends('layouts.admin')
@section('content')
    <h1 class="text-center p-3">{{__('Update Item')}}</h1>
    <div class="row">
        <div class="col-md-6">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="{{route('admin.groups.update',$group->id)}}">
                @method('PUT')
                @csrf
                <div class="mb-3">
                    <label for="name" class="form-label">{{__('Group name')}}</label>
                    <input type="text" class="form-control" id="name" name="name"  value="{{$group->name}}">
                </div>
                <button type="submit" class="btn btn-primary">{{__('Update')}}</button>
                <button id="myBtn" class="btn btn-primary">{{__('Update with Ajax')}}</button>

            </form>
        </div>
    </div>
            <div class="result"></div>


    <script>
        document.getElementById("myBtn").addEventListener("click", TableEditable);

        function TableEditable(){
            let result = document.querySelector('.result');
            let name = document.querySelector('#name');

            // Creating a XHR object
            let xhr = new XMLHttpRequest();
            let url = "{{route('admin.groups.update',$group->id)}}";

            // open a connection
            xhr.open("POST", url, true);

            // Set the request header i.e. which type of content you are sending
            xhr.setRequestHeader("Content-Type", "application/json");

            // Create a state change callback
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 && xhr.status === 200) {

                    // Print received data from server
                    result.innerHTML = this.responseText;

                }
            };

            // Converting JSON data to string
            var data = JSON.stringify({ "name": name.value });

            // Sending data with the request
            xhr.send(data);
        }
    </script>
@endsection



@extends('layouts.admin')
@section('content')
    <h1 class="text-center p-3">Information about Group</h1>
    <div class=""><a href="{{route('admin.groups.index')}}" class="btn bg-danger text-white m-3 "><b class="font-size-20">&#8249&#8249&nbsp</b>{{__('Back')}}</a></div>
    <div class="container row  " style="height: 60vh">
        <div class="col-md-6  " style="height: 50%">
            <table class="table">
                <tr>
                    <td>{{__('Group Name')}}</td>
                    <td>{{$group->name}}</td>
                </tr>
            </table>
        </div>
    </div>
@endsection



@extends('layouts.admin')
@section('content')
    <h1 class="text-center p-3">{{__('Create Item')}}</h1>
    <div class="row d-flex justify-content-center">
        <div class="col-md-4">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="{{route('admin.groups.store')}}">
                @csrf
                <div class="mb-3">
                    <label for="name" class="form-label">{{__('Warehouse name')}}</label>
                    <input type="text" class="form-control" id="name" name="name"  value="{{old('name')}}">
                </div>
                <div class="mb-3">
                    <label for="address" class="form-label">{{__('Address')}}</label>
                    <input type="text" class="form-control" id="address" name="address"  value="{{old('address')}}">
                </div>
                <div class="mb-3">
                    <label for="group_id" class="form-label">{{__('Group name')}}</label>
                    <input type="text" class="form-control" id="group_id" name="group_id"  value="{{old('group_id')}}">
                </div>
                <button type="submit" class="btn btn-primary">{{__('Submit')}}</button>
            </form>
        </div>
    </div>
@endsection



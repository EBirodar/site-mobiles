@extends('layouts.admin ')
@section('content')
    <div class="container">
        <table class="table">
            <caption>List of Groups</caption>
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">{{_('name')}}</th>
                <th colspan="3" class="text-center" scope="col">{{__('Actions')}}</th>

            </tr>
            </thead>

            <tbody>
            <div class="d-flex flex-row-reverse"><a href="{{route('admin.groups.create')}}" class="btn bg-primary text-white m-3 ">{{__('Add Item')}}</a></div>

            @foreach($groups as $group)
                <tr>
                    <th scope="row">{{($groups->currentpage()-1)*$groups->perpage()+$loop->index+1}}</th>
                    <td>{{$group->name}}</td>
                    <td>
                        <a href="{{route('admin.groups.show',$group->id)}}">
                            <i class="fa fa-eye"  aria-hidden="true"></i>
                        </a>
                    </td>
                    <td>
                        <a href="{{route('admin.groups.edit',['group'=>$group->id])}}">
                            <i class="fas fa-pencil-alt" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td>
                        <form method="post" action="{{route('admin.groups.destroy',['group'=>$group->id])}}">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger">

                                <i class="fas fa-trash-alt" aria-hidden="true"></i>
                            </button>

                        </form>

                    </td>
                </tr>
            @endforeach
            </tbody>

        </table>

        {{$groups->links()}}
    </div>
@endsection



<div class="vertical-menu">

  <!-- LOGO -->
  <div class="navbar-brand-box" style="height: 70px; display: flex; justify-content: flex-start; align-items: center;">
    <a href="{{ Route('admin.dashboard') }}" class="logo logo-dark">
      {{-- <span class="logo-sm">
        <img src="/assets/images/logo-sm.png" alt="" height="22" style="height: 22px;">
      </span> --}}
      <span class="logo-lg">
        <img src="/assets/images/logo-dark.png" alt="" height="20" style="height: 20px;">
      </span>
    </a>

    <a href="index.html" class="logo logo-light">
      <span class="logo-sm">
        <img src="/assets/images/logo-sm.png" alt="" height="22" style="height: 22px;">
      </span>
      <span class="logo-lg">
        <img src="/assets/images/logo-light.png" alt="" height="20" style="height: 20px;">
      </span>
    </a>
  </div>

  <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect vertical-menu-btn">
    <i class="fa fa-fw fa-bars"></i>
  </button>

  <div data-simplebar class="sidebar-menu-scroll">

    <!--- Sidemenu -->
    <div id="sidebar-menu">
      <!-- Left Menu Start -->
      <ul class="metismenu list-unstyled" id="side-menu">
        <li class="menu-title">Menu</li>

        <li>
          <a href="{{ Route('admin.dashboard') }}">
            <i class="uil-home-alt"></i><span class="badge rounded-pill bg-primary float-end">01</span>
            <span>Dashboard</span>
          </a>
        </li>

        <li class="menu-title">Apps</li>

        <li>
          <a href="javascript: void(0);" class="has-arrow waves-effect">
            <i class="uil-users-alt"></i>
            <span>admin</span>
          </a>
          <ul class="sub-menu" aria-expanded="false">
            <li><a href="{{ Route('admin.groups.index') }}">Group</a></li>
            <li><a href="ecommerce-product-detail.html">Product Detail</a></li>
            <li><a href="ecommerce-orders.html">Orders</a></li>
          </ul>
        </li>
        <li>
          <a href="javascript: void(0);" class="has-arrow waves-effect">
            <i class="uil-users-alt"></i>
            <span>Users</span>
          </a>
          <ul class="sub-menu" aria-expanded="false">
            <li><a href="{{ Route('admin.users') }}">List</a></li>
            <li><a href="ecommerce-product-detail.html">Product Detail</a></li>
            <li><a href="ecommerce-orders.html">Orders</a></li>
          </ul>
        </li>

        <li>
          <a href="javascript: void(0);" class="has-arrow waves-effect">
            <i class="uil-store"></i>
            <span>{{__('Products')}}</span>
          </a>
          <ul class="sub-menu" aria-expanded="false">
            <li><a href="ecommerce-products.html">{{__('mahsulot kirim boyicha')}}</a></li>
            <li><a href="ecommerce-product-detail.html">{{__('mahsulot umumiy soni boyicha ')}}</a></li>
            <li><a href="ecommerce-orders.html">{{__('mahsulot umumiy skald boyicha')}}</a></li>
            <li><a href="ecommerce-orders.html">{{__('Mahsulot otkazish')}}</a></li>
            <li><a href="ecommerce-orders.html">{{__('Qoldiq mahsulotning tahminiy hisobi')}}</a></li>
            <li><a href="ecommerce-orders.html">{{__('Mahsulot tarixi')}}</a></li>
            <li><a href="ecommerce-orders.html">{{__('Mahsulot tarixi model boyicha')}}</a></li>
            <li><a href="ecommerce-orders.html">{{__('Mahsulot kirim boyicha hisobot')}}</a></li>
            <li><a href="ecommerce-orders.html">{{__('Mahsulot qidirish')}}</a></li>
            <li>
                    <a href="javascript:void(0);" class="has-arrow waves-effect" onclick="myFunction()">
                        <span >{{__('Model')}}</span>
                    </a>
                    <div class="topnav" id="myLinks" style="display: none;">
                        <a href="#news">{{__('Tur')}}</a>
                        <a href="{{route('admin.brands.index')}}">{{__('Brand')}}</a>
                        <a href="#about">{{__('Mark')}}</a>
                    </div>

            </li>
            <li><a href="ecommerce-orders.html">{{__('Mahsulot qoshish')}}</a></li>
          </ul>
        </li>


          <li>
              <a href="javascript: void(0);" class="has-arrow waves-effect">
                  <i class="uil-store"></i>
                  <span>{{__('Pul amaliyotlari')}}</span>
              </a>
              <ul class="sub-menu" aria-expanded="false">
                  <li><a href="ecommerce-products.html">{{__('Pul otkazma')}}</a></li>
                  <li><a href="ecommerce-product-detail.html">{{__('Pul qoldiq')}}</a></li>
                  <li><a href="ecommerce-product-detail.html">{{__('Valyuta ayirboshlash')}}</a></li>
                  <li><a href="ecommerce-product-detail.html">{{__('Hisobot')}}</a></li>
                  <li>
                      <a href="javascript: void(0);" class="has-arrow waves-effect">
                          <i class="uil-store"></i>
                          <span>{{__('Harajat')}}</span>
                          <ul class="sub-menu" aria-expanded="false">
                              <li><a href="ecommerce-products.html">{{__('Harajatlar tarixi')}}</a></li>
                          </ul>
                      </a>
                  </li>
              </ul>
          </li>
          <li>
              <a href="javascript: void(0);" class="has-arrow waves-effect">
                  <i class="uil-store"></i>
                  <span>{{__('Clients')}}</span>
              </a>
              <ul class="sub-menu" aria-expanded="false">
                  <li><a href="ecommerce-product-detail.html">{{__('Haqdor mijozlar')}}</a></li>
                  <li><a href="ecommerce-product-detail.html">{{__('Debtor clients')}}</a></li>
                  <li>
                      <a href="javascript: void(0);" class="has-arrow waves-effect">
                          <i class="uil-store"></i>
                          <span>{{__('Term payment')}}</span>
                          <ul class="sub-menu" aria-expanded="false">
                              <li><a href="ecommerce-products.html">{{__('Muddatli tolov hisoboti')}}</a></li>
                          </ul>
                      </a>
                  </li>
              </ul>
          </li>

        <li>
          <a href="javascript: void(0);" class="has-arrow waves-effect">
            <i class="uil-shopping-cart-alt"></i>
            <span>{{__('Boshqaruv')}}</span>
          </a>
          <ul class="sub-menu" aria-expanded="false">
            <li><a href="ecommerce-products.html">{{__('Ombor')}}</a></li>
            <li><a href="ecommerce-product-detail.html">{{__('Xodimlar')}}</a></li>
          </ul>
        </li>

        <li>
          <a href="javascript: void(0);" class="has-arrow waves-effect">
            <i class="uil-shopping-cart-alt"></i>
            <span>{{__('Narx')}}</span>
          </a>
          <ul class="sub-menu" aria-expanded="false">
            <li><a href="ecommerce-products.html">{{__('Narx turi')}}</a></li>
            <li><a href="ecommerce-product-detail.html">{{__('Narx chiqarish')}}</a></li>
            <li><a href="ecommerce-product-detail.html">{{__('Narxlarni yuborish')}}</a></li>
          </ul>
        </li>

        <li>
          <a href="javascript: void(0);" class="has-arrow waves-effect">
            <i class="uil-shopping-cart-alt"></i>
            <span>{{__('Hisobot Savdo')}}</span>
          </a>
          <ul class="sub-menu" aria-expanded="false">
            <li><a href="ecommerce-products.html">{{__('Reyting')}}</a></li>
            <li><a href="ecommerce-product-detail.html">{{__('Kunlik Savdo Kassa')}}</a></li>
            <li><a href="ecommerce-product-detail.html">{{__('Guruhlar boyicha')}}</a></li>
            <li><a href="ecommerce-product-detail.html">{{__('Brend boyicha')}}</a></li>
            <li><a href="ecommerce-product-detail.html">{{__('Kun boyicha')}}</a></li>
            <li><a href="ecommerce-product-detail.html">{{__('Kunlik brend boyicha')}}</a></li>
            <li>
                <a href="javascript: void(0);" class="has-arrow waves-effect">
                    <i class="uil-shopping-cart-alt"></i>
                    <span>{{__('Savdo')}}</span>
                </a>
                <ul class="sub-menu" aria-expanded="false">
                    <li><a href="ecommerce-products.html">{{__('Umumiylashtirish')}}</a></li>
                </ul>
            </li>
          </ul>
        </li>

          <li>
              <a href="javascript: void(0);" class="has-arrow waves-effect">
                  <i class="uil-shopping-cart-alt"></i>
                  <span>{{__('Zakaz')}}</span>
              </a>
              <ul class="sub-menu" aria-expanded="false">
                  <li><a href="ecommerce-products.html">{{__('Ombordagi sorayotgan tovarlar boyicha')}}</a></li>
                  <li><a href="ecommerce-product-detail.html">{{__('Narx chiqarish')}}</a></li>
                  <li><a href="ecommerce-product-detail.html">{{__('Narxlarni yuborish')}}</a></li>
              </ul>
          </li>


          <li>
          <a href="javascript: void(0);"
            class="
            @if (!count(auth()->user()->unreadnotifications) > 0) has-arrow @endif
              waves-effect">
            <i class="uil-comments-alt"></i>

            @if (count(auth()->user()->unreadnotifications) > 0)
              <span class="badge rounded-pill bg-warning float-end">New</span>
            @endif
            <span>Messages</span>
          </a>
          <ul class="sub-menu" aria-expanded="false">
            <li><a href="{{ Route('admin.notifications') }}">
                @if (count(auth()->user()->unreadnotifications) > 0)
                  <span
                    class="badge rounded-pill bg-primary float-end">{{ count(auth()->user()->unreadnotifications) }}</span>
                @endif
                Notification
              </a></li>
            <li><a href="ecommerce-product-detail.html">Product Detail</a></li>
            <li><a href="ecommerce-orders.html">Orders</a></li>
          </ul>
        </li>

      </ul>
    </div>
    <!-- Sidebar -->
  </div>
</div>


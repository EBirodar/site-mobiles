<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('money_transfers', function (Blueprint $table) {
            $table->id();
            $table->integer('money_invoice_id');
            $table->integer('currency_id');
            $table->double('money_sys');
            $table->double('money_get');
            $table->integer('main');
            $table->integer('status');
            $table->foreign('money_invoice_id')
                ->references('id')
                ->on('money_invoices');
            $table->foreign('currency_id')
                ->references('id')
                ->on('currencies');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('money_transfers');
    }
};

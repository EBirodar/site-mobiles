<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->integer('invoice_id');
            $table->integer('warehouse_id');
            $table->integer('client_id');
            $table->integer('currency_id');
            $table->integer('user_id');
            $table->integer('trade_type');
            $table->foreign('invoice_id')
                ->references('id')
                ->on('invoices');
            $table->foreign('warehouse_id')
                ->references('id')
                ->on('warehouses');
            $table->foreign('client_id')
                ->references('id')
                ->on('clients');
            $table->foreign('currency_id')
                ->references('id')
                ->on('currencies');
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_type_warehouses', function (Blueprint $table) {
            $table->id();
            $table->integer('price_type_id');
            $table->integer('warehouse_id');
            $table->integer('currency_id');
            $table->integer('mark_id');
            $table->double('bonus');
            $table->double('price');
            $table->foreign('price_type_id')
                ->references('id')
                ->on('price_types');
            $table->foreign('warehouse_id')
                ->references('id')
                ->on('warehouses');
            $table->foreign('currency_id')
                ->references('id')
                ->on('currencies');
            $table->foreign('mark_id')
                ->references('id')
                ->on('marks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_type_warehouses');
    }
};

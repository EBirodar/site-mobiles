<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_requests', function (Blueprint $table) {
            $table->id();
            $table->integer('mark_id');
            $table->integer('warehouse_from_id');
            $table->integer('warehouse_to_id');
            $table->integer('count');
            $table->dateTime('date');
            $table->foreign('mark_id')
                ->references('id')
                ->on('marks');
            $table->foreign('warehouse_from_id')
                ->references('id')
                ->on('warehouses');
            $table->foreign('warehouse_to_id')
                ->references('id')
                ->on('warehouses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_requests');
    }
};

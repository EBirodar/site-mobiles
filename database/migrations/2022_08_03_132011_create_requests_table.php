<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->id();
            $table->integer('mark_id');
            $table->integer('warehouse_from_id');
            $table->integer('warehouse_to_id');
            $table->integer('pre_request_id');
            $table->integer('count');
            $table->dateTime('date');
            $table->foreign('warehouse_from_id')
                ->references('id')
                ->on('warehouses');
            $table->foreign('warehouse_to_id')
                ->references('id')
                ->on('warehouses');
            $table->foreign('pre_request_id')
                ->references('id')
                ->on('pre_requests')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
};

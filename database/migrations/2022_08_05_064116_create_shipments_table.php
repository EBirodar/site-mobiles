<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipments', function (Blueprint $table) {
            $table->id();
            $table->integer('client_id');
            $table->integer('warehouse_id');
            $table->foreignId('invoice_id');
            $table->integer('user_id');
            $table->integer('quantity');
            $table->foreign('client_id')
                ->references('id')
                ->on('clients');
            $table->foreign('warehouse_id')
                ->references('id')
                ->on('warehouse');
            $table->foreign('invoice_id')
                ->references('id')
                ->on('invoices');
            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipments');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->integer('mark_id');
            $table->integer('warehouse_id');
            $table->integer('shipment_id');
            $table->integer('quantity');
            $table->integer('order_count');
            $table->text('note');
            $table->double('price');
            $table->double('expense');
            $table->foreign('mark_id')
                ->references('id')
                ->on('marks');
            $table->foreign('warehouse_id')
                ->references('id')
                ->on('warehouses');
            $table->foreign('shipment_id')
                ->references('id')
                ->on('shipments');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
